export const todos = [
  {
    id: 1,
    content: 'Clean the house',
    isDone: true,
  },
  {
    id: 2,
    content: 'Deploy to APP',
    isDone: false,
  },
];

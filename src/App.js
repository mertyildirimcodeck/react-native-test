import React, {useState} from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const App = () => {
  const [todos, setTodos] = useState([]);
  const [todoContent, setTodoContent] = useState('');

  const addTodo = () => {
    setTodos([
      ...todos,
      {
        id: todos.length + 1,
        content: todoContent,
        isDone: false,
      },
    ]);
  };
  return (
    <SafeAreaView style={styles.root}>
      <View style={styles.body}>
        <FlatList
          keyExtractor={(_, index) => index.toString()}
          data={todos}
          renderItem={({item}) => <Text>{item.content}</Text>}
        />
      </View>
      <View style={styles.action}>
        <TextInput
          placeholder={'Add Todo Here'}
          onChangeText={text => setTodoContent(text)}
        />
        <TouchableOpacity onPress={addTodo}>
          <Text>Add</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  body: {
    flex: 1,
  },
  action: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
});

export default App;

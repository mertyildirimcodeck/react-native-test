/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../src/App';

import {fireEvent, render} from '@testing-library/react-native';

it('user can add item', () => {
  const {getByPlaceholderText, getByText, getAllByText} = render(<App />);

  fireEvent.changeText(
    getByPlaceholderText('Add Todo Here'),
    'Write some texts',
  );

  fireEvent.press(getByText('Add'));

  const todoElements = getAllByText('Write some texts');
  expect(todoElements).toHaveLength(1);
});
